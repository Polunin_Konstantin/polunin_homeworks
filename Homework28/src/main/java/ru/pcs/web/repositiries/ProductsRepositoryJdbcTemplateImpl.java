package ru.pcs.web.repositiries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Products;

import javax.sql.DataSource;
import java.util.List;

@Component
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into products(description, cost, quantity) values(?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from products order by id";
    //language=SQL
    private static final String SQL_SELECT_BY_COST = "select * from products where cost=";


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    private static final RowMapper<Products> productsRowMapper = (row, rowNumber)->{
        int id = row.getInt("id");
        String description = row.getString("description");
        int cost = row.getInt("cost");
        int quantity = row.getInt("quantity");
        return new Products(id, description, cost, quantity);
    };

    @Override
    public List<Products> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productsRowMapper);
    }

    @Override
    public List<Products> findByCost(int cost) {

        return jdbcTemplate.query(SQL_SELECT_BY_COST+cost, productsRowMapper);
    }

    @Override
    public void save(Products products) {
        jdbcTemplate.update(SQL_INSERT, products.getDescription()/*, products.getCost(), products.getQuantity()*/);
    }
}
