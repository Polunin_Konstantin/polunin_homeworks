package ru.pcs.web.repositiries;

import ru.pcs.web.models.Products;

import java.util.List;

public interface ProductsRepository {
    List<Products> findAll();
    List<Products> findByCost(int cost);
    void save(Products products);



}
