package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.models.Products;
import ru.pcs.web.repositiries.ProductsRepository;

@Controller
public class ProductsController {
    @Autowired
    private ProductsRepository productsRepository;

    @PostMapping("/products")
    public String addProduct(@RequestParam ("description") String description){
                             //@RequestParam("cost") Integer cost,
                             //@RequestParam("quantity") Integer quantity)
        Products products = Products.builder()

                        .description(description)
                        .build();
        productsRepository.save(products);



        //System.out.println(description);
        return "redirect:/products_add.html";
    }

}
