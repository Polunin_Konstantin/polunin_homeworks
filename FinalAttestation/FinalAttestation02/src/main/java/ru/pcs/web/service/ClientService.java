package ru.pcs.web.service;

import ru.pcs.web.forms.ClientForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Ordering;

import java.util.List;

public interface ClientService {
    void addClient(ClientForm form);
    List<Client> getAllClients();
    void deleteClient(Integer clientId);
    Client getClient(Integer clientId);

    List<Ordering> getOrderingsByClient(Integer clientId);

    void update(Integer clientId, ClientForm clientForm);
}


