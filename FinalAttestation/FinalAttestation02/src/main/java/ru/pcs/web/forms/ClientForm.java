package ru.pcs.web.forms;

import lombok.Data;

@Data
public class ClientForm {
    private String firstName;
    private String lastName;
    private String email;

}
