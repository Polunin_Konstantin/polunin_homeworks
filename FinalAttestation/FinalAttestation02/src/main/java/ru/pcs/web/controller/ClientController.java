package ru.pcs.web.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.ClientForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Ordering;
import ru.pcs.web.service.ClientService;

import java.util.List;

@Controller
public class ClientController {

    private final ClientService clientService;

@Autowired
    public ClientController(ClientService clientService) {
        this.clientService = clientService;
    }

    @GetMapping("/clients")
    public String getClientsPage(Model model) {
        List<Client> clients = clientService.getAllClients();
        model.addAttribute("clients", clients);
        return "clients";
    }
    @GetMapping("/clients/{client-id}")
    public String getClientPage(Model model, @PathVariable("client-id") Integer clientId) {
        Client client = clientService.getClient(clientId);
        model.addAttribute("client", client);
        return "client";
    }

    @PostMapping("/clients")
    public String addClient(ClientForm form){
        clientService.addClient(form);
        return "redirect:/clients";

    }
    @PostMapping("/clients/{client-id}/delete")
    public String deleteClient(@PathVariable("client-id") Integer clientId){
        clientService.deleteClient(clientId);
        return "redirect:/clients";
    }
    @PostMapping("/clients/{client-id}/update")
        public String update(@PathVariable("client-id") Integer clientId, ClientForm clientForm){
        clientService.update(clientId, clientForm);
        return "redirect:/clients";
    }

    @GetMapping("/clients/{client-id}/orderings")
    public String getOrderingsByClient(Model model, @PathVariable("client-id") Integer clientId){
        List<Ordering> orderings = clientService.getOrderingsByClient(clientId);
        model.addAttribute("orderings", orderings);
        return "orderings_of_client";

    }



}
