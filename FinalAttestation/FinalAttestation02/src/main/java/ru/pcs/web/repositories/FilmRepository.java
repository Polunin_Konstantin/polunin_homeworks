package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Film;

import java.util.List;

public interface FilmRepository extends JpaRepository<Film, Integer> {

}
