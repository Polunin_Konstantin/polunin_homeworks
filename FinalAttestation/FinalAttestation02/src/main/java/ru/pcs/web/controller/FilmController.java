package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import ru.pcs.web.forms.ClientForm;
import ru.pcs.web.forms.OrderingForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Film;
import ru.pcs.web.models.Ordering;
import ru.pcs.web.service.ClientService;
import ru.pcs.web.service.FilmService;

import java.util.List;

@Controller
public class FilmController {

    private final FilmService filmService;
    private  final ClientService clientService;

    @Autowired
    public FilmController(FilmService filmService, ClientService clientService) {
        this.filmService = filmService;
        this.clientService = clientService;
    }

    @GetMapping("/films")
    public String getClientsPage(Model model) {
        List<Film> films = filmService.getAllFilms();
        List<Client> clients = clientService.getAllClients();
        model.addAttribute("films", films);
        model.addAttribute("clients", clients);
        return "films";
    }
    @PostMapping("/films")
    public String addOrder(OrderingForm form) {
        filmService.addOrder(form);
//        clientService.addClient(form2);
        return "redirect:/films";
    }

}
