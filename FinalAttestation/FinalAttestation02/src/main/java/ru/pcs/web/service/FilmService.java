package ru.pcs.web.service;


import ru.pcs.web.forms.OrderingForm;
import ru.pcs.web.models.Film;

import java.util.List;

public interface FilmService {
    List<Film> getAllFilms();
    void addOrder(OrderingForm form);

}
