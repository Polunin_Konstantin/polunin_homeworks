package ru.pcs.web.forms;

import lombok.Data;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Film;

import java.time.LocalDate;


@Data
public class OrderingForm {
    private Film filmId;
    private Client clientId;
    private String date;

}
