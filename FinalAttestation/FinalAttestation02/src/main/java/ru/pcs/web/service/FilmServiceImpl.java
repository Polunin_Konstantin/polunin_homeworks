package ru.pcs.web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.OrderingForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Film;
import ru.pcs.web.models.Ordering;
import ru.pcs.web.repositories.FilmRepository;
import ru.pcs.web.repositories.OrderingRepository;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Component
public class FilmServiceImpl implements FilmService {

    private final FilmRepository filmRepository;
    private final OrderingRepository orderingRepository;

    @Override
    public List<Film> getAllFilms() {
        return filmRepository.findAll();
    }

    @Override
    public void addOrder(OrderingForm form) {
        Ordering ordering = Ordering.builder()
                .film(form.getFilmId())
                .client(form.getClientId())
                .date(LocalDate.parse("2021-12-27"))
                .build();
        orderingRepository.save(ordering);



    }
}
