package ru.pcs.web.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.ClientForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.models.Ordering;
import ru.pcs.web.repositories.ClientRepository;
import ru.pcs.web.repositories.OrderingRepository;

import java.util.List;

@RequiredArgsConstructor
@Component
public class ClientServiceImpl implements ClientService {

    private final ClientRepository clientRepository;
    private final OrderingRepository orderingRepository;



    @Override
    public void addClient(ClientForm form) {
        Client client = Client.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .email(form.getEmail())
                .build();
        clientRepository.save(client);

    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public void deleteClient(Integer clientId) {
        clientRepository.deleteById(clientId);
    }

    @Override
    public Client getClient(Integer clientId) {
        return clientRepository.getById(clientId);
    }

    @Override
    public List<Ordering> getOrderingsByClient(Integer clientId) {
        return orderingRepository.findAllByClient_Id(clientId);
    }

    @Override
    public void update(Integer clientId, ClientForm clientForm) {
        Client client = clientRepository.getById(clientId);
        client.setFirstName(clientForm.getFirstName());
        client.setLastName(clientForm.getLastName());
        clientRepository.save(client);
    }
}
