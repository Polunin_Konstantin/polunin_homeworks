/*package ru.pcs.web.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Client;


import javax.sql.DataSource;
import java.util.List;

@Component
public class ClientRepositoryJdbcTemplateImpl implements ClientRepository {

    //language=SQL
    private static final String SQL_INSERT = "insert into client(first_name, last_name, email) values(?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from client order by id";
    //language=SQL
    private static final String SQL_DELETE_BY_ID = "delete from client where id = ?;";
    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select* from client where id = ?";


    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ClientRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    private static final RowMapper<Client> clientRowMapper = (row, rowNumber)->{
        int id = row.getInt("id");
        String first_name = row.getString("first_name");
        String last_name = row.getString("last_name");
        String email = row.getString("email");
        return new Client(id, first_name, last_name, email);
    };

    @Override
    public List<Client> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, clientRowMapper);
    }

    @Override
    public void save(Client client) {
        jdbcTemplate.update(SQL_INSERT, client.getFirstName(), client.getLastName(), client.getEmail());
    }

    @Override
    public Client findById(Integer clientId) {
        return jdbcTemplate.queryForObject(SQL_SELECT_BY_ID, clientRowMapper, clientId);
    }

    @Override
    public void delete(Integer clientId) {
        jdbcTemplate.update(SQL_DELETE_BY_ID, clientId);
    }
}
*/