package ru.pcs.web.service;


import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ru.pcs.web.forms.SignUpForm;
import ru.pcs.web.models.Client;
import ru.pcs.web.repositories.ClientRepository;


@RequiredArgsConstructor
@Component
public class SignUpServiceImpl implements SignUpService {
    private final PasswordEncoder passwordEncoder;
    private final ClientRepository clientRepository;

    @Override
    public void signUpUser(SignUpForm form) {
        Client client = Client.builder()
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .role(Client.Role.CLIENT)
                .email(form.getEmail())
                .hashPassword(passwordEncoder.encode(form.getPassword()))
                .build();
        clientRepository.save(client);


    }
}
