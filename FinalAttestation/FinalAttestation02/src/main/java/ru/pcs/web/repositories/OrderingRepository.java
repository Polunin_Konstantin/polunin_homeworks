package ru.pcs.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.models.Ordering;

import java.util.List;

public interface OrderingRepository extends JpaRepository<Ordering, Integer> {
    List<Ordering> findAllByClient_Id(Integer id);

}
