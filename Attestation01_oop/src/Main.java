import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;


public class Main {

    public static void main(String[] args) throws IOException {
	    UsersRepository usersRepository = new UsersRepositoryFileImpl("users5.txt");
        List <User> users = usersRepository.findAll();
        int id = 1;
        User user = usersRepository.findById(id);
        user.setAge(27);
        user.setName("Марсель");
        users.set(id-1, user);

        Writer writer = new FileWriter("users5.txt", false);

        for (User user1: users )
            usersRepository.update(user1);


    }
}
