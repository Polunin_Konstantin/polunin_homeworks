package ru.pcs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;


import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd{
        @ParameterizedTest(name = "return {2} on {0} and {1}")
        @CsvSource(value = {"16, 18, 2", "13, 1, 1", "13, 15, 1", "0, 5, 5", "5, 0, 5"})
        public void return_correct_gcd(int a, int b, int result){
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throw exception on {0}")
        @ValueSource(ints = {-1})
        public void bad_numbers_throw_exception(int badNumber){
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(badNumber, badNumber));
        }
    }






}