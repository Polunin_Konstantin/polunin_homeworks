package ru.pcs;

public class NumbersUtil {
    public int gcd(int a, int b) {
        if (a<0 || b<0)
            throw new IllegalArgumentException();

        if (b==0) return a;
        return gcd(b,a%b);

    }
}
