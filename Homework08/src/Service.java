public class Service {
    private Human[] humans;

    public Service(Human[] humans) {
        this.humans = humans;
    }

    public Human[] sort() {
        for (int i = 0; i < humans.length; i++) {
            for (int j = 0; j < humans.length - i - 1; j++) {
                if (humans[j].getWeight() > humans[j + 1].getWeight()) {
                    Human temp = humans[j];
                    humans[j] = humans[j + 1];
                    humans[j + 1] = temp;
                }
            }
        }
        return humans;

    }
}
