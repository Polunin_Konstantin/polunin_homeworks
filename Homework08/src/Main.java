import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        Human[] humans = new Human[10];
        Human human1 = new Human("Олег", 90.0);
        Human human2 = new Human("Борис", 75.0);
        Human human3 = new Human("Ира", 55.0);
        Human human4 = new Human("Юля", 45.0);
        Human human5 = new Human("Миша", 78.0);
        Human human6 = new Human("Никита", 63.0);
        Human human7 = new Human("Варя", 35.0);
        Human human8 = new Human("Виктор", 87.0);
        Human human9 = new Human("Саша", 97.0);
        Human human10 = new Human("Женя", 100);

        humans[0] = human1;
        humans[1] = human2;
        humans[2] = human3;
        humans[3] = human4;
        humans[4] = human5;
        humans[5] = human6;
        humans[6] = human7;
        humans[7] = human8;
        humans[8] = human9;
        humans[9] = human10;



        Service Service = new Service(humans);
        Service.sort();

        for (int i = 0; i< humans.length; i++)
            System.out.println(humans[i]);


    }
}
