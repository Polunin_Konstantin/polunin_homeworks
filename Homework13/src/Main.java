public class Main {

    public static void main(String[] args) {
    ByCondition condition1 = number -> {
        if ( number%2==0)
            return true;
        return false;
    };
    ByCondition condition2 = number -> {
        int sum = 0;
        while (number>0){
            sum = sum + number%10;
            number /=10;
        }
        if (sum%2 == 0)
            return true;
        return false;
    };


    int[] array = {25, 986, 752, 4};


        for (int i : Sequence.filter(array, condition1)) System.out.print(i + " ");
        System.out.println();

        for (int j : Sequence.filter(array, condition2)) System.out.print(j + " ");
    }



}
