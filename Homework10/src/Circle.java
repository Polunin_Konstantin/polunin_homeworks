public class Circle extends Ellipse implements Translate {
    public Circle(int x, int y, int r1) {
        super(x, y, r1, r1);


    }

    @Override
    public int moveX(int x) {
        return x;
    }

    @Override
    public int moveY(int y) {
        return y;
    }
}
