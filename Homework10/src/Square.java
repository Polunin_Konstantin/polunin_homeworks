public class Square extends Rectangle implements Translate{

    public Square(int x, int y, int a) {
        super(x, y, a, a);
    }

    @Override
    public int moveX(int x) {
        return x;
    }

    @Override
    public int moveY(int y) {
        return y;
    }
}
