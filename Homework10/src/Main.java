public class Main {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(0,0,5, 3);
        Ellipse ellipse = new Ellipse(0, 0, 9, 3);
        Circle circle = new Circle(0,0,5);
        Square square = new Square(0,0,4);
        //String[] arrayFigure = new String[2];
        int pointX = 5;
        int pointY = 4;


        circle.x = circle.moveX(pointX);
        circle.y = circle.moveY(pointY);
        square.x = square.moveX(pointX);
        square.y = square.moveY(pointY);

        System.out.println("Circle: x = " + circle.x + ", y = " + circle.y);
        System.out.println("Square: x = " + square.x + ", y = " + square.y);

        }
}
