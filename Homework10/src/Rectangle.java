public class Rectangle extends Figure{

    protected int a;
    private int b;

    public Rectangle(int x, int y, int a, int b) {
        super(x,y);
        this.a = a;
        this.b = b;
    }
    public int getPerimeter(){
        return a*2+b*2;

    }

}


