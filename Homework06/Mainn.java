public class Mainn{

	/**
	 * принимает на вход массив и целое число. 
	 * Возвращает индекс этого числа в массиве.
	 * Если число в массиве отсутствует - возвращает -1.
	 * 
	 *
	 */

	public static int getIndex(int[] array, int b){
		int[] array2;
		int j = 0;

		for(int i = 0; i < array.length; i++){

			if(array[i] == b)
				return i;	
		}
		return -1;
	}


	public static void print(int[] array){
		for(int i = 0; i < array.length; i++){
			if(array[i]==0){
				int zero =i;
				for(int j=zero; j < array.length-1; j++){
					array[j] = array[j+1];
				}
				array[array.length-1]=0;
					
			}
			System.out.print(array[i] + " ");
		}
		System.out.println();
	}

	


	public static void main(String[] args){
		int[] a = {2, 0, 9, 0, -8, 25, 7, 65, 6};
		int result = getIndex(a, 6);
		System.out.println(result);
		print(a);


	}
}