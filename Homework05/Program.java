import java.util.Scanner;

class Program{
	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		int a = scanner.nextInt();
		int minDigit = a;

		while (a!=-1){
			while (a!=0){
				int currentDigit = a%10;
				if(currentDigit<minDigit)
					minDigit = currentDigit;
				a=a/10;
			}
			a = scanner.nextInt();
		}
		System.out.println(minDigit);
	}
}