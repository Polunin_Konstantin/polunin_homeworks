create table products (
                          id serial primary key,
                          description varchar(50),
                          cost integer,
                          quantity integer check (quantity >=0)
);

insert into products(description, cost, quantity) values ('Часы', 50, 20);
insert into products(description, cost, quantity) values ('Телефон', 130, 50);
insert into products(description, cost, quantity) values ('Пылесос', 20, 10);
insert into products(description, cost, quantity) values ('Весы', 10, 100);
insert into products(description, cost, quantity) values ('Телефон', 150, 50);

update products set quantity = 150 where id = 4;
alter table products alter column cost type integer;
select  * from products;
select description from products;
select description, quantity from products order by quantity;
select description, cost from products order by cost desc;
select description, cost from products order by cost;
update products set cost = 150 where id = 2;
select * from products where cost<100;
select count(*) from products where quantity>30;

create table customer
(
    id serial primary key ,
    first_name varchar(20),
    last_name varchar(20)
);
insert into customer(first_name, last_name)values ('Иван', 'Петров');
insert into customer(first_name, last_name)values ('Тимур', 'Иванов');
insert into customer(first_name, last_name)values ('Евгений', 'Захаров');
insert into customer(first_name, last_name)values ('Ольга', 'Игнатьева');
insert into customer(first_name, last_name) values ('Никита', 'Овдеев');

create table ordering(
                         product_id integer,
                         customer_id integer,
                         foreign key (product_id) references products(id),
                         foreign key (customer_id) references customer(id),
                         date date,
                         quantity integer
);
insert into ordering(product_id, customer_id, date, quantity) values  (2, 1, '2021-10-15', 1);
insert into ordering(product_id, customer_id, date, quantity) values  (1, 1, '2021-10-15', 2);
insert into ordering(product_id, customer_id, date, quantity) values  (4, 4, '2021-11-30', 1);
insert into ordering(product_id, customer_id, date, quantity) values  (5, 4, '2021-05-10', 1);
insert into ordering(product_id, customer_id, date, quantity) values  (3, 2, '2021-01-07', 1);
insert into ordering(product_id, customer_id, date, quantity) values  (3, 3, '2021-02-02', 3);

-- получить имя/фамилию пользователя с id 3 и количество купленного товара с id 3
select first_name, last_name , (select count(*) from ordering where product_id = 3) as id1_count from customer where id = 3;
-- получить имя/фамилию пользователей и количество их заказов
select first_name, last_name , (select count(*) from ordering where ordering.customer_id = customer.id) as orderings_count from customer;
-- Получить имя/фамилию пользователей, которые совершили заказ 2021-10-15
select first_name, last_name from customer where  customer.id in(select customer_id from ordering where date = '2021-10-15');





