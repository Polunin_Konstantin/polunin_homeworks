import java.util.List;

public interface ProductsRepository {
    List<Products> findAll();
    List<Products> findByCost(int cost);



}
