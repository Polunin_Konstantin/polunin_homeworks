import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class Products {
    private int id;
    private String description;
    private int cost;
    private int quantity;


}
