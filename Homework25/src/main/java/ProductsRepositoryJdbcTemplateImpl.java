import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {
    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from products order by id";
    //language=SQL
    private static final String SQL_SELECT_BY_COST = "select * from products where cost=";


    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    private static final RowMapper<Products> productsRowMapper = (row, rowNumber)->{
        int id = row.getInt("id");
        String description = row.getString("description");
        int cost = row.getInt("cost");
        int quantity = row.getInt("quantity");
        return new Products(id, description, cost, quantity);
    };

    @Override
    public List<Products> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, productsRowMapper);
    }

    @Override
    public List<Products> findByCost(int cost) {

        return jdbcTemplate.query(SQL_SELECT_BY_COST+cost, productsRowMapper);
    }
}
