import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/pcs_2", "postgres", "asd123");
        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);
        System.out.println(productsRepository.findAll());
        System.out.println(productsRepository.findByCost(150));



    }
}
