public class Main {

    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle(5, 3);
        Ellipse ellipse = new Ellipse(9, 3);
        Circle circle = new Circle(5);
        Square square = new Square(4);

        System.out.println("Rectangle - " + rectangle.getPerimeter());
        System.out.println("Ellipse - " + ellipse.getPerimeter());
        System.out.println("Circle - " + circle.getPerimeter());
        System.out.println("Square - " + square.getPerimeter());


        }
}
