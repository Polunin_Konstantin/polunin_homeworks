public class Ellipse extends Figure{

    protected int r1;
    private int r2;

    public Ellipse(int r1, int r2) {

        this.r1 = r1;
        this.r2 = r2;
    }

    public double getPerimeter(){
        if (r1>=r2)
            return (4*3.14*r1*r2+r1-r2)/(r1+r2);
        return 0;

    }
}
