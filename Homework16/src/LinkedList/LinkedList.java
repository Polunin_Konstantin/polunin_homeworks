package LinkedList;

public class LinkedList<T>{

    public static class Node<T> {
        T value;
        Node<T> next;

        public Node(T value) {
            this.value = value;
        }
    }
    private Node<T> first;
    private Node<T> last;
    private int size;

    public void add(T element){
        Node<T> newNode = new Node<>(element);
        if(size==0){
            first = newNode;
        }
        else{
            last.next = newNode;
        }
        last = newNode;
        size++;
    }

    public boolean isCorrectIndex(int index){
        return  index >=0 && index < size;

    }

    public int size(){
        return size;
    }

    public T get(int index) {
        if (!isCorrectIndex(index)) {
            throw new IndexOutOfBoundsException("Out of exception");

        }
        Node<T> temp = first;
        for(int i = 0; i < index; i++){
            temp=temp.next;
        }

        return temp.value;





    }
}



