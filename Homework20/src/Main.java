import java.util.List;



public class Main {

    public static void main(String[] args) {
        CarsRepository carsRepository = new CarsRepository("cars.txt");
        List<Car> cars = carsRepository.findAll();
        cars.stream()
                .filter(car -> car.getColor().equals("Black") || car.getKm()==0)
                .map(car -> car.getNumber())
                .forEach(car-> System.out.println(car));
    }


}
