import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class CarsRepository {
    private String fileName;

    public CarsRepository(String fileName) {
        this.fileName = fileName;
    }

    public List<Car> findAll() {
        List<Car> cars = new ArrayList<>();
        try (Reader reader = new FileReader(fileName); BufferedReader bufferedReader = new BufferedReader(reader)) {

            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                String number = parts[0];
                String model = parts[1];
                String color = parts[2];
                int km = Integer.parseInt(parts[3]);
                int price = Integer.parseInt(parts[4]);
                Car newCar = new Car(number, model, color, km, price);
                cars.add(newCar);
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


        return cars;
    }

}
